﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace testApp
{
    class Database
    {
        static SqlConnection Connection = new SqlConnection("Data Source=DESKTOP-JPL8NAR;Initial Catalog=Test_Project;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

        public Database()
        {
            Connection.Open();
        }

        public void CreateTable()
        {
            SqlCommand command = new SqlCommand("NewTable", Connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            try
            {
                command.ExecuteNonQuery();
                Console.WriteLine("Таблица успешно создана");
            }
            catch(Exception e)
            {
                Console.WriteLine("Таблица уже существует");
            }
            
        }

        public void AddMan(string FIO,DateTime date,string Gender)
        {
            SqlCommand command = new SqlCommand("AddMan", Connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.Add("@name", SqlDbType.VarChar, 50).Value=FIO;
            command.Parameters.Add("@date", SqlDbType.DateTime, 50).Value=date;
            command.Parameters.Add("@gender", SqlDbType.VarChar, 50).Value=Gender;
            try
            {
                command.ExecuteNonQuery();
                Console.WriteLine("Запись успешно добавлена");
            }
            catch(Exception e)
            {
                Console.WriteLine("Ошибка!Таблица не создана");
            }


        }

        public void selectUnique()
        {
            SqlCommand command = new SqlCommand("selectunique", Connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}", reader.GetName(1), reader.GetName(3), reader.GetName(2), "Age");
                                 

                    while (reader.Read()) // построчно считываем данные
                    {
                        object name = reader.GetValue(0);
                        object date = reader.GetValue(1);
                        object gender = reader.GetValue(2);
                        object age = reader.GetValue(3);

                        Console.WriteLine("{0} \t{1} \t{2} \t{3}", name, gender, date, age);
                    }
                
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка!Таблица не создана");
            }
        }

        public void selectF()
        {
            SqlCommand command = new SqlCommand("selectF", Connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            

            SqlDataReader reader = command.ExecuteReader();
            try
            {
                Console.WriteLine("{0}\t{1}\t{2}", reader.GetName(1), reader.GetName(3), reader.GetName(2));

                    while (reader.Read()) 
                    {
                        object name = reader.GetValue(1);
                        object date = reader.GetValue(2);
                        object gender = reader.GetValue(3);
                        

                        Console.WriteLine("{0} \t{1} \t{2}", name, gender, date);
                    }
               

                
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка!Таблица не создана");
            }
        }

        public void AddRandom()
        {
            int gender;
            int day;
            int month;
            int year;
            int surname;
            Random random = new Random();
            DateTime date;
            string name;
            string sgender;
            for(int i = 0;i<100000;i++)
            {
                gender = random.Next(0,2);
                day = random.Next(1,29);
                month = random.Next(1,13);
                year = random.Next(1920, 2019);
                surname = random.Next(26);

                date = DateTime.Parse(day + "." + month + "." + year);

                char temp = (char)(surname+65);
                name = temp.ToString();
                if (gender == 1)
                    sgender = "Male";
                else
                    sgender = "Female";
                

                SqlCommand command = new SqlCommand("AddMan", Connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                command.Parameters.Add("@date", SqlDbType.DateTime, 50).Value = date;
                command.Parameters.Add("@gender", SqlDbType.VarChar, 50).Value = sgender;
                try
                {
                    command.ExecuteNonQuery();
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine("Ошибка!Таблица не создана");
                }

            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Database database = new Database();
           //database.selectF();
            try
            {
                switch (args[0])
                {
                    case "1":
                        database.CreateTable();
                        break;

                    case "2":
                        database.AddMan(args[1], DateTime.Parse(args[2]), args[3]);
                        break;

                    case "3":
                        database.selectUnique();
                        break;

                    case "4":
                        database.AddRandom();
                        break;

                    case "5":
                        database.selectF();
                        break;


                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Ошибка!Не передан парметр");
            }
            
        }
    }
}
