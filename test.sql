CREATE DATABASE Test_Project
GO
USE [Test_Project]
GO
/****** Object:  Table [dbo].[People]    Script Date: 14.03.2019 19:48:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[People](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FIO] [varchar](50) NULL,
	[Date] [datetime] NULL,
	[Gender] [varchar](50) NULL,
 CONSTRAINT [PK_People_id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[AddMan]    Script Date: 14.03.2019 19:48:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddMan]
	@name nvarchar(50),
	@date datetime,
  @gender nvarchar(50)
AS
	INSERT INTO People 
	VALUES(@name,@date,@gender)
GO
/****** Object:  StoredProcedure [dbo].[NewTable]    Script Date: 14.03.2019 19:48:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NewTable]
AS 
CREATE TABLE Test_Project.dbo.People (
  Id int IDENTITY,
  FIO varchar(50) NULL,
  Date datetime NULL,
  Gender varchar(50) NULL,
  CONSTRAINT PK_People_id PRIMARY KEY CLUSTERED (Id)
)
GO
/****** Object:  StoredProcedure [dbo].[selectF]    Script Date: 14.03.2019 19:48:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selectF]
AS
SELECT * FROM People
  WHERE People.FIO LIKE 'F%' AND People.Gender='Male'

GO
/****** Object:  StoredProcedure [dbo].[selectunique]    Script Date: 14.03.2019 19:48:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selectunique]
AS 
SELECT People.FIO,People.Date,People.Gender,DATEDIFF(YEAR,People.Date,GETDATE()) AS Age
  FROM People
GROUP BY People.FIO,People.Date,People.Gender
ORDER BY People.FIO
GO
